from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
import numpy as np
from textblob import TextBlob
import matplotlib.pyplot as plt


def getInputData(inputFile):
    with open(inputFile,"r") as filePointer:
        inputData = filePointer.readlines()
    return inputData

def getStopWords(language):
    return set(stopwords.words(language))

def getPairWiseSegments(inputData):
    index = -1
    pairWiseTextSegments = []
    sentenceTokenisedList = []
    for i in range(len(inputData)):
        if i % 2 == 0:
            index += 1
            pairWiseTextSegments.append([])
            sentenceTokenisedList.append([])
            pairWiseTextSegments[index].append(inputData[i].rstrip())
            sentenceTokenisedList[index].append(inputData[i].rstrip())
        else:
            pairWiseTextSegments[index].append(inputData[i].rstrip())
            sentenceTokenisedList[index].append(inputData[i].rstrip())
    return pairWiseTextSegments, sentenceTokenisedList

def getWordTokenised(sentenceTokenisedList):
    wordTokenisedList = []
    for i in range(len(sentenceTokenisedList)):
        wordTokenisedList.append([])
        wordTokenisedList[i].append(word_tokenize(sentenceTokenisedList[i][0]))
        wordTokenisedList[i].append(word_tokenize(sentenceTokenisedList[i][1]))
    return wordTokenisedList

def getPairWiseCorpus(pairWiseTextSegments):
    pairWiseCorpus = []
    for i in pairWiseTextSegments:
        pairWiseCorpus.append(''.join(i))
    return pairWiseCorpus

def getPunctuationFreeList(inputString):
    tokenizer = RegexpTokenizer(r'\w+')          
    punctuationFreeList = tokenizer.tokenize(inputString)
    return punctuationFreeList

def getPunctuationFreeCorpus(pairWiseCorpus):
    punctuationFreeCorpus = []
    for i in pairWiseCorpus:
        punctuationFreeCorpus.append(getPunctuationFreeList(i))
    return punctuationFreeCorpus

def getStopWordFreeList(stopWordsList,puncutationFreeList):
    stopWordFreeList = []
    for i in puncutationFreeList:
        if i.lower() not in stopWordsList and len(i.lower()) != 1:
            stopWordFreeList.append(i)
    return stopWordFreeList

def getStopWordFreeCorpus(punctuationFreeCorpus):
    stopWordList = getStopWords('english')
    stopWordFreeCorpus = []
    for i in punctuationFreeCorpus:
        stopWordFreeCorpus.append(getStopWordFreeList(stopWordList, i))
    return stopWordFreeCorpus

def getLemmatizedCorpus(stopWordFreeCorpus):
    lemmatizedCorpus = []
    lemmatizer = WordNetLemmatizer()
    for i in range(len(stopWordFreeCorpus)):
        lemmatizedCorpus.append([])
        for j in  stopWordFreeCorpus[i]:
            lemmatizedCorpus[i].append(lemmatizer.lemmatize(j))
    return lemmatizedCorpus

def getWordDifferenceIndex(stopWordFreeCorpus, lemmatizedCorpus):
    wordDifferenceIndex = {}
    for i in range(len(stopWordFreeCorpus)):
        for j in range(len(stopWordFreeCorpus[i])):
            if stopWordFreeCorpus[i][j] != lemmatizedCorpus[i][j]:
                if i not in wordDifferenceIndex:
                    wordDifferenceIndex[i] = []
                    wordDifferenceIndex[i].append([stopWordFreeCorpus[i][j],lemmatizedCorpus[i][j]])
                else:
                    wordDifferenceIndex[i].append([stopWordFreeCorpus[i][j],lemmatizedCorpus[i][j]])
    return wordDifferenceIndex

def getWordIndexOfPairWiseData(wordTokenisedList):
    stopWordsList = getStopWords('english')
    counter = 0
    wordIndexOfPairWiseData = {}
    for i in range(len(wordTokenisedList)):
        wordIndexOfPairWiseData[i] = {0:{},1:{}}
        for j in range(len(wordTokenisedList[i])):
            for k in range(len(wordTokenisedList[i][j])):
                if (wordTokenisedList[i][j][k] not in wordIndexOfPairWiseData[i][j]) \
                    and (wordTokenisedList[i][j][k].lower() not in stopWordsList) \
                    and len(wordTokenisedList[i][j][k]) != 1:
                    wordIndexOfPairWiseData[i][j].update(
                        {wordTokenisedList[i][j][k] : k})
    return wordIndexOfPairWiseData

def plotBarGraph(list1, list2):
    N = len(list1)
    menMeans = list1
    ind = np.arange(N)  # the x locations for the groups
    width = 0.35       # the width of the bars

    fig = plt.figure()
    ax = fig.add_subplot(111)
    rects1 = ax.bar(ind, menMeans, width, color='royalblue')

    womenMeans = list2
    womenStd =   (3, 5, 2, 3, 3)
    rects2 = ax.bar(ind+width, womenMeans, width, color='seagreen')

    # add some
    ax.set_ylabel('Scores')
    ax.set_title('Scores by group and gender')
    ax.set_xticks(ind + width / 2)
    ax.set_xticklabels( ('G1', 'G2', 'G3', 'G4', 'G5') )

    ax.legend( (rects1[0], rects2[0]), ('Men', 'Women') )

    plt.show()



def getGraph(beforePreProcessing, afterPreProcessing):
    beforePreProcessingPloarity = []
    afterPreProcessingPloarity = []
    beforePreProcessingSubjectivity = []
    afterPreProcessingSubjectivity = []
    for i in range(len(beforePreProcessing)):
        beforePreProcessingPloarity.append(beforePreProcessing[i][0])
        afterPreProcessingPloarity.append(afterPreProcessing[i][0])
        beforePreProcessingSubjectivity.append(beforePreProcessing[i][1])
        afterPreProcessingSubjectivity.append(afterPreProcessing[i][1])
    plotBarGraph(beforePreProcessingPloarity, afterPreProcessingPloarity)
    


def lemmatizingEffect(wordDifferenceIndex, wordTokenisedList, wordIndexOfPairWiseData):
    beforePreProcessing = []
    afterPreProcessing = []
    for key,values in wordDifferenceIndex.items():
        inputSentence1 = ' '.join(wordTokenisedList[key][0])
        inputSentence2 = ' '.join(wordTokenisedList[key][1])
        sentence1 = -1
        sentence2 = -1
        for i in range(len(values)):
                if values[i][0] in wordIndexOfPairWiseData[key][0]:
                    sentence1 = 1
                    wordTokenisedList[key][0][wordIndexOfPairWiseData[key][0][values[i][0]]] = values[i][1]
                elif values[i][0] in wordIndexOfPairWiseData[key][1]:
                    sentence2 = 1
                    wordTokenisedList[key][1][wordIndexOfPairWiseData[key][1][values[i][0]]] = values[i][1]
        if sentence1 == 1:
            
            
            beforePreProcessing.append(TextBlob(inputSentence1).sentiment)
            afterPreProcessing.append(TextBlob(' '.join(wordTokenisedList[key][0])).sentiment)
        if sentence2 == 1:
            

            beforePreProcessing.append(TextBlob(inputSentence2).sentiment)
            afterPreProcessing.append(TextBlob(' '.join(wordTokenisedList[key][1])).sentiment)
        
    getGraph(beforePreProcessing, afterPreProcessing)
            

def getSemanticRelation(inputFile):
    inputData = getInputData(inputFile)
    pairWiseTextSegments, sentenceTokenisedList = getPairWiseSegments(inputData)
    wordTokenisedList = getWordTokenised(sentenceTokenisedList)
    pairWiseCorpus = getPairWiseCorpus(pairWiseTextSegments)
    wordIndexOfPairWiseData = getWordIndexOfPairWiseData(wordTokenisedList)
    punctuationFreeCorpus = getPunctuationFreeCorpus(pairWiseCorpus)
    stopWordFreeCorpus = getStopWordFreeCorpus(punctuationFreeCorpus)
    lemmatizedCorpus = getLemmatizedCorpus(stopWordFreeCorpus)
    wordDifferenceIndex = getWordDifferenceIndex(stopWordFreeCorpus, lemmatizedCorpus)
    lemmatizingEffect(wordDifferenceIndex, wordTokenisedList, wordIndexOfPairWiseData)

if __name__ == "__main__":
    inputFile = "Dataset.txt"
    getSemanticRelation("Dataset.txt")